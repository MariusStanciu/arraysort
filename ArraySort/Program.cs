﻿using System;
using System.Diagnostics;

namespace ArraySort
{
    class Program
    {
        static void Main(string[] args)
        {
            // var arr1 = new int[] { 7, 2, 5, 9, 1, 3, 4 };

            // Console.WriteLine(ArrayDistinct.SortByRemove(arr1)); // 4

            // var arr2 = new int[] { 2, 5, 3, 1 };

            // Console.WriteLine(ArrayDistinct.SortByRemove(arr2)); // 2

            var arr3 = new int[] { 87, 95, 88, 84, 99, 96, 93, 81, 89, 98, 75, 77, 71, 79, 72, 73, 66, 55, 69, 54, 67, 57, 62, 51, 41, 48, 46, 47, 49, 32, 31, 34, 39, 37, 36, 22, 15, 24, 19, 26, 23, 27, 11, 1, 18, 7, 17, 6, 3, 5, 13 };
            var arr4 = new int[] { 19, 26, 23, 27, 11, 1, 18, 7, 17, 6, 3, 5, 13 };

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine(ArrayDistinct.SortByRemove(arr4));
            stopWatch.Stop();

            // Stopwatch stopWatch2 = new Stopwatch();
            // stopWatch2.Start();
            // Console.WriteLine(ArrayDistinct.SortByRemove(arr3));
            // stopWatch2.Stop();

            Console.WriteLine("Sort Method: " + stopWatch.Elapsed);
            // Console.WriteLine("SortRemove Method: " + stopWatch2.ElapsedMilliseconds);
        }
    }
}
