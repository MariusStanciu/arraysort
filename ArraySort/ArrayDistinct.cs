using System.Linq;
using System;

namespace ArraySort
{
    public class ArrayDistinct
    {
        public static int Sort(int[] arr)
        {
            int steps = 0;

            for (int i = 1; i < arr.Length; i++)
            {
                var min = arr.Skip(i).Min();

                if (arr[i - 1] > min) 
                {
                    var index = Array.IndexOf(arr, min);
                   
                    arr[index] = arr[i - 1];
                    arr[i - 1] = min;

                    steps++;
                }
            }

            return steps;
        }

        public static int SortByRemove(int[] arr)
        {
            int steps = 0;

            while(arr.Length > 1)
            {
                var min = arr.Skip(1).Min();

                if (arr[0] > min)
                {
                    var index = Array.IndexOf(arr, min);
                    arr[index] = arr[0];

                    steps++;
                }

                arr = arr.Skip(1).ToArray();
            }

            return steps;
        }
    }
}