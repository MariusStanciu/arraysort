using FluentAssertions;
using Xunit;

namespace ArraySort.Tests
{
    public class ArrayDistinctSortByRemoveTests
    {
        [Fact]
        public void SortByRemove_ReturnsIntGreaterOrEqualWith0_WhenParamIsIntArray()
        {
            var arr = new int[] { 5, 8 , 6 };

            var result = ArrayDistinct.SortByRemove(arr);

            result.Should().BeOfType(typeof(int));
            result.Should().BeGreaterOrEqualTo(0);
        }

        [Theory]
        [InlineData(new int[] { 7, 2, 5, 9, 1, 3, 4 }, 4)]
        [InlineData(new int[] { 2, 5, 3, 1 }, 2)]
        [InlineData(new int[] { 1, 2, 3, 4 }, 0)]
        [InlineData(new int[] { 1 }, 0)]
        public void SortByRemove_ReturnsInt_WhenParamIsIntArray(int[] arr, int expected)
        {
            var result = ArrayDistinct.Sort(arr);

            result.Should().Be(expected);
        }
    }
}